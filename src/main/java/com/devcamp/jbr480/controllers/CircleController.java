package com.devcamp.jbr480.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr480.models.Circle;

@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getArea(@RequestParam ( value = "r") double r) {
        Circle vongtron1 = new Circle(r);
        double area =vongtron1.getArea();
        return area;

    }
    @GetMapping("/circle-perimeter")
    public double getPerimeter(@RequestParam ( value = "r") double r) {
        Circle vongtron1 = new Circle(r);
        double perimeter =vongtron1.getPerimeter();
        return perimeter;

    }
}
