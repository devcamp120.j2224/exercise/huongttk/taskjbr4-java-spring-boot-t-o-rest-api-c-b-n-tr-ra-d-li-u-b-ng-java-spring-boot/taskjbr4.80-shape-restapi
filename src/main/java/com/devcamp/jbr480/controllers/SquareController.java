package com.devcamp.jbr480.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr480.models.Square;

@RestController
public class SquareController {
    @CrossOrigin
    @GetMapping("/square-area")
    public double getArea(@RequestParam ( value = "w") double w) {
        Square hinhvuong = new Square(w);
        double area =hinhvuong.getArea();
        return area;
    }
    @GetMapping("/square-perimeter")
    public double getPerimeter(@RequestParam ( value = "w") double w) {
        Square hinhvuong1 = new Square(w);
        double area =hinhvuong1.getPerimeter();
        return area;

    }
}
