package com.devcamp.jbr480.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr480.models.Rectangle;

@RestController
public class RectangleController {
    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getArea(@RequestParam ( value = "w") double w,@RequestParam ( value = "h") double h) {
        Rectangle hinhvuong1 = new Rectangle(w, h);
        double area =hinhvuong1.getArea();
        return area;

    }
    @GetMapping("/rectangle-perimeter")
    public double getPerimeter(@RequestParam ( value = "w") double w,@RequestParam ( value = "h") double h) {
        Rectangle hinhvuong1 = new Rectangle(w, h);
        double area =hinhvuong1.getPerimeter();
        return area;

    }
}
