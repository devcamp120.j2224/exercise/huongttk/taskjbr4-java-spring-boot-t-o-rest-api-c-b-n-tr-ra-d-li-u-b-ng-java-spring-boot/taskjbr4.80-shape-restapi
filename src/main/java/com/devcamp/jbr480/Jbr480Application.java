package com.devcamp.jbr480;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr480Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr480Application.class, args);
	}

}
